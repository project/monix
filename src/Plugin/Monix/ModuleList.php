<?php

namespace Drupal\monix\Plugin\Monix;

use Drupal\Core\Utility\ProjectInfo;
use Drupal\monix\MonixPluginBase;

/**
 * Return list of all modules and themes with their version.
 *
 * @Monix(
 *   id = "module_list",
 *   title = @Translation("Module list"),
 *   description = @Translation("Return list of all modules and themes with their version."),
 *   type = "json",
 *   path = "module_list"
 * )
 */
class ModuleList extends MonixPluginBase {

  /**
   * {@inheritdoc}
   */
  public function result() {
    $data = [];
    $projects = [];
    $module_data = \Drupal::service('extension.list.module')->reset()->getList();
    $theme_data = \Drupal::service('theme_handler')->rebuildThemeData();
    $project_info = new ProjectInfo();
    $project_info->processInfoList($projects, $module_data, 'module', TRUE);
    $project_info->processInfoList($projects, $theme_data, 'theme', TRUE);

    foreach ($projects as $project) {
      $data[$project['name']] = $project['info']['version'];
    }

    return $data;
  }

}
