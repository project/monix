<?php

namespace Drupal\monix\Plugin\Monix;

use Drupal\monix\MonixPluginBase;

/**
 * Return timestamp of last cron run.
 *
 * @Monix(
 *   id = "last_cron_run",
 *   title = @Translation("Last cron run"),
 *   description = @Translation("Return timestamp of last cron run."),
 *   type = "timestamp",
 *   path = "last_cron_run"
 * )
 */
class LastCronRun extends MonixPluginBase {

  /**
   * {@inheritdoc}
   */
  public function result() {
    return \Drupal::state()->get('system.cron_last');
  }

}
