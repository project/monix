<?php

namespace Drupal\monix\Plugin\Monix;

use Drupal\monix\MonixPluginBase;

/**
 * Return total amount of users.
 *
 * @Monix(
 *   id = "total_users",
 *   title = @Translation("Total users"),
 *   description = @Translation("Return total amount of users."),
 *   type = "int",
 *   path = "total_users"
 * )
 */
class TotalUsers extends MonixPluginBase {

  /**
   * {@inheritdoc}
   */
  public function result() {
    $ids = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->execute();

    return count($ids);
  }

}
