<?php

namespace Drupal\monix\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Drupal\monix\MonixPluginManager;

/**
 * Defines dynamic routes.
 */
class Routes implements ContainerInjectionInterface {

  /**
   * Route endpoint name prefix.
   *
   * @var string
   */
  const ROUTE_NAME_PREFIX = 'monix.resource';

  /**
   * Route endpoint path prefix.
   *
   * @var string
   */
  const ROUTE_PATH_PREFIX = 'monix';

  /**
   * Monix plugin manager.
   *
   * @var \Drupal\monix\MonixPluginManager
   */
  protected $monixPluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(MonixPluginManager $monix_plugin_manager) {
    $this->monixPluginManager = $monix_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.monix')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routes = new RouteCollection();

    $plugin_definitions = $this->monixPluginManager->getDefinitions();

    foreach ($plugin_definitions as $plugin_id => $plugin_definition) {
      if (!empty($plugin_definition['path'])) {
        $name = implode('.', [self::ROUTE_NAME_PREFIX, $plugin_id]);
        $path = implode('/', [self::ROUTE_PATH_PREFIX, $plugin_definition['path']]);
        $routes->add($name, new Route(
          $path, ['plugin_id' => $plugin_id]
        ));
      }
    }

    $routes->addDefaults(['_controller' => '\Drupal\monix\Controller\MonixController::response']);
    $routes->addRequirements(['_custom_access' => '\Drupal\monix\Controller\MonixController::access']);

    return $routes;
  }

}
