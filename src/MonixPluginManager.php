<?php

namespace Drupal\monix;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a Monix plugin manager.
 *
 * @see \Drupal\Core\Archiver\Annotation\Archiver
 * @see \Drupal\Core\Archiver\ArchiverInterface
 * @see plugin_api
 */
class MonixPluginManager extends DefaultPluginManager {

  /**
   * Constructs a ArchiverManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Monix',
      $namespaces,
      $module_handler,
      'Drupal\monix\MonixInterface',
      'Drupal\monix\Annotation\Monix'
    );
    $this->alterInfo('monix_info');
    $this->setCacheBackend($cache_backend, 'monix_info_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $plugin_definition = $this->getDefinition($plugin_id);
    $plugin_class = DefaultFactory::getPluginClass($plugin_id, $plugin_definition, 'Drupal\monix\MonixPluginBaseInterface');
    return $plugin_class::create(\Drupal::getContainer());
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    if (empty($options['plugin_id'])) {
      return NULL;
    }
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if ($options['plugin_id'] === $plugin_id) {
        return $this->createInstance($plugin_id);
      }
    }
  }

}
