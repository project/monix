<?php

namespace Drupal\monix\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\monix\MonixPluginManager;
use Drupal\monix\Routing\Routes;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Component\Utility\Xss;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Monix endpoint controller.
 */
class MonixController extends ControllerBase {

  /**
   * Monix plugin manager.
   *
   * @var \Drupal\monix\MonixPluginManager
   */
  protected $monixPluginManager;

  /**
   * MonixController constructor.
   *
   * @param \Drupal\monix\MonixPluginManager $monix_plugin_manager
   *   The monix_plugin_manager.
   */
  public function __construct(MonixPluginManager $monix_plugin_manager) {
    $this->monixPluginManager = $monix_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.monix')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function response($plugin_id) {
    $plugin_instance = $this->monixPluginManager->getInstance([
      'plugin_id' => $plugin_id,
    ]);

    // Plugin not fount for some reason.
    if (is_null($plugin_instance)) {
      throw new NotFoundHttpException();
    }

    // Get the result data from plugin instance.
    $result = $plugin_instance->result();

    // When result from plugin is an array output this as JSON.
    if (is_array($result)) {
      return new JsonResponse($result);
    }

    // If it's null or string return plain page.
    return new Response(Xss::filter($result));
  }

  /**
   * {@inheritdoc}
   */
  public function pluginsList() {
    $list = [];
    $monix_manager = \Drupal::service('plugin.manager.monix');

    foreach ($monix_manager->getDefinitions() as $plugin_id => $plugin_definition) {
      if (!empty($plugin_definition['path'])) {
        $list[] = [
          'name' => $plugin_id,
          'type' => isset($plugin_definition['type']) ? $plugin_definition['type'] : 'string',
          'path' => implode('/', [
            Routes::ROUTE_PATH_PREFIX,
            $plugin_definition['path'],
          ]),
        ];
      }
    }

    return new JsonResponse($list);
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account) {
    $url_token = \Drupal::request()->headers->get('X-Access-Token');
    $access_token = \Drupal::config('monix.settings')->get('access_token');
    if (!empty($access_token) && $url_token === $access_token) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

}
