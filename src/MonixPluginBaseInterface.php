<?php

namespace Drupal\monix;

/**
 * Defines the common interface for all Monix classes.
 */
interface MonixPluginBaseInterface {

  /**
   * Return resource data in format defined in plugin annotation.
   *
   * @return null|string|array
   *   Return monitored value response.
   */
  public function result();

}
