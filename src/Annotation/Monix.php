<?php

namespace Drupal\monix\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines Monix annotation object.
 *
 * @Annotation
 */
class Monix extends Plugin {

  /**
   * Plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var string
   * @ingroup plugin_translatable
   *
   *  \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var string
   * @ingroup plugin_translatable
   *
   *  \Drupal\Core\Annotation\Translation
   */
  public $description;

  /**
   * Plugin return data type.
   *
   * @var string
   */
  public $type;

  /**
   * Path suffix to the Monix endpoint.
   *
   * @var string
   */
  public $path;

}
