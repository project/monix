<?php

namespace Drupal\monix\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Monix settings for this site.
 *
 * @internal
 */
class MonixSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'monix_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['monix.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $monix_config = $this->config('monix.settings');

    $form['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#default_value' => $monix_config->get('access_token'),
      '#description' => $this->t('Protect monitoring endpoints with secure token'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('monix.settings')
      ->set('access_token', $form_state->getValue('access_token'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
